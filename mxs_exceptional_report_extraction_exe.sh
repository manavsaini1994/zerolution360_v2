#!/usr/bin/sh
#***************************************************************************************
# Maxis Telecom
# Filename: mxs_exceptional_report_extraction_exe.sh
# ******************	Version : 1.0 **************************************************
# Execution Syntax: nohup mxs_exceptional_report_extraction_exe.sh sql_folder db_name &
# Example:nohup mxs_exceptional_report_extraction_exe.sh PROJECT_GREEN_ENT FXPGSM1 &
#****************************************************************************************

##----- Report Main Configuration File ----------------------------
#. /home/RPTDUMP/repdev/Zerolution360/LOADER/CODE/init.ini
#. /apps/arbor/custom/conf/report_properties.ini
#. /rptmig/Zerolution360/LOADER/CODE/init.ini
. /apps/arbor/custom/Zerolution360_V2/EXCEPTIONAL_REPORT/CODE/init.ini

##----- Program main variables section ----------------------------
sdate=`date +"%Y%m%d"`
pdate=`date +"%d%m%Y"`
properties_file=$report_sql/${1}/extraction_setup.properties


#****************************************************************************************
# CHECK INPUT PARAMETERS
#****************************************************************************************
if [  $# -eq 1  ] ; then
    
    sql_folder=$1
    db_name=$main_db_names
    
    logfile=$report_log/mxs_mig_extraction_${sql_folder}_${pdate}.log
    sql_listing=${report_code}/list_extraction_${sql_folder}.temp

elif [  $# -eq 2  ] ; then
    
    sql_folder=$1
    db_name=$2
    
    logfile=$report_log/mxs_mig_extraction_${sql_folder}_${db_name}_${pdate}.log
    sql_listing=${report_code}/list_extraction_${sql_folder}_${db_name}.temp

elif [  $# -eq 3  ] ; then
    
    sql_folder=$1
    db_name=$2
    start_date=$3
    logfile=$report_log/mxs_mig_extraction_${sql_folder}_${db_name}_${pdate}.log
    sql_listing=${report_code}/list_extraction_${sql_folder}_${db_name}.temp

elif [  $# -eq 4  ] ; then
    
    sql_folder=$1
    db_name=$2
    start_date=$3
    end_date=$4
    
    logfile=$report_log/mxs_mig_extraction_${sql_folder}_${db_name}_${pdate}.log
    sql_listing=${report_code}/list_extraction_${sql_folder}_${db_name}.temp
    
elif [  $# -eq 5  ] ; then
    
    sql_folder=$1
    db_name=$2
    start_date=$3
    end_date=$4
    db_link=$5
    
    logfile=$report_log/mxs_mig_extraction_${sql_folder}_${db_name}_${db_link}_${pdate}.log
    sql_listing=${report_code}/list_extraction_${sql_folder}_${db_name}_${db_link}.temp
    
else

    logfile=$report_log/mxs_mig_extraction_${1}_${pdate}.log
    
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | Invalid number of arguments passed" >> $logfile
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Terminated" >> $logfile

    echo "Invalid Parameter | Usage : mxs_exceptional_report_extraction_exe.sh <migration_project> [db_name] [start_date] [end_date]"
	exit 1
fi

echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | Script Started" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | SQL Folder : $sql_folder" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | DB Name : ${db_name[@]}" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | DB Link : ${db_link}" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | Start Date : ${start_date}" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | End Date : ${end_date}" >> $logfile


#****************************************************************************************
# Program Start
#****************************************************************************************
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | Maxis Extraction Program - Start" >> $logfile


#****************************************************************************************
# Validate if the EXTRACTION_SQL Folder and the SQL File Exists
#****************************************************************************************
if [ ! -d ${report_sql}/${sql_folder} ] ; then
    echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | NO ${report_sql}/${sql_folder} folder found!" >> $logfile
    echo "${sql_folder} folder is NOT found in ${report_sql} directory!!!"

    exit 1
fi

if [ -f ${properties_file} ] ; then

    ls -1 ${report_sql}/${sql_folder} | grep .sql > ${sql_listing}
    ## Check if any SQL script in the folder
    count_list_tx=`wc -l ${sql_listing} | awk '{ print$1 }'`
    if [ $count_list_tx -eq 0 ]; then
        echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | There is no SQL file found in ${report_sql}/${sql_folder} folder" >> $logfile

        exit 1
    fi

else 

    echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | Extraction Properties file is not found in ${report_sql}/${sql_folder}" >> $logfile
    echo "extraction_setup.properties is NOT found in ${report_sql}/${sql_folder} directory!!!"

    exit 1
fi


#****************************************************************************************
# Extraction Process Function
#****************************************************************************************
function extraction_process
{

    db=${1}
    echo "`date +"%Y-%m-%d.%H:%M:%S"` | START TIME | Extracting $script_name in $db Started" >> $logfile

    batch_err_msg=`sqlplus -s /nolog << EOF >> ${extraction_output}.tmp
        set heading off
        set tab off
        set trimspool on
        set trimout on
        set pagesize 0
        set feedback off
        set verify off
        set linesize 1500
        set arraysize 1500

        connect ${auth_1}/${auth_2}@${db}

        @${report_sql}/${sql_folder}/${script_name} $sql_folder $db $start_date $end_date $db_link
    exit;
    EOF
    `

    #****************************************************************************************
    # ERROR HANDLING SECTION
    #****************************************************************************************
    out=`egrep -e 'ORA-|CPY|ERROR' ${extraction_output}.tmp`
    if [ $? -eq 0 ]; then

        echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | SQL ERROR - SQL File Execution: $script_name" >> $logfile
        echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | SQL ERROR - Error Message: \n $out" >> $logfile
        
        exit 2
    fi

    echo "`date +"%Y-%m-%d.%H:%M:%S"` | END TIME | Extracting $script_name in $db Completed" >> $logfile
}


echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | SQL Script Extraction Started" >> $logfile
#****************************************************************************************
# SQL EXECUTION
#****************************************************************************************

while read script_name
do
    echo "`date +"%Y-%m-%d.%H:%M:%S"` | START TIME | SQL File Execution : "${script_name} >> $logfile
   	    filename=`awk -F\|  '{ var2=$2; print var2 }' ${properties_file}`
	    extension=`awk -F\|  '{ var2=$3; print var2 }' ${properties_file}`
	    split_count=`awk -F\|  '{ var2=$4; print var2 }' ${properties_file}`
	    header=`awk -F\|  '{ var2=$5; print var2 }' ${properties_file}`

    extraction_output=${report_data}/${filename}${pdate}.${extension}

    echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | Extraction Output : "${extraction_output} >> $logfile
    echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | File Spliter Count : "${split_count} >> $logfile
    echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | File Header : "${header} >> $logfile

    if [ $split_count -eq 0 ] && [[ -n $header ]]; then		
        echo $header >> ${extraction_output}		
    fi
    for db_conn in ${db_name[@]}
    do
        extraction_process $db_conn &
        
        extraction_process_list[${i}]=$!
        ((i=i+1))
    done

    wait_process 'extraction_process' $logfile ${extraction_process_list[@]}

    grep -v "Connected." ${extraction_output}.tmp >> ${extraction_output}
    rm ${extraction_output}.tmp

    #split the file via loop
    templine=`wc -l ${extraction_output}`
    seq=1
    ln_count=1

    while [ $split_count -gt 0 ] && [ $ln_count -lt $templine ]
    do
        # set the file sequence in final_filename
        pad=`printf "%03d" $seq`
        final_filename=${report_data}/${filename}_${pdate}_${pad}.${extension}

        # header
        if [[ -n $header ]]; then
            echo $header >> ${final_filename}
        fi

        # Body data,
        tail +${ln_count} $extraction_output | head -${split_count} >> $final_filename

        # Trailer

        # Counter
        ((seq=seq + 1))
        ((ln_count=ln_count+split_count))

    done

    if [ $split_count -gt 0 ]; then
        rm -r $extraction_output
    fi

    echo "`date +"%Y-%m-%d.%H:%M:%S"` | END TIME | SQL File Execution : "${script_name} >> $logfile

done < ${sql_listing}

wait

rm -r ${sql_listing}

# End Looping for SQL Execution
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | SQL Script Extraction Completed" >> $logfile


# Program Completed
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Maxis Extraction Program - Completed" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Completed" >> $logfile
#***************************************************************************
# END OF EXTRACTION PROGRAM
#***************************************************************************
