DECLARE
  CURSOR CUR_TABLE_LIST
  IS
    SELECT TABLE_NAME
    FROM USER_TABLES
    WHERE UPPER(TABLE_NAME) IN ( 'MXS_ACT_SD_TERM_ZERO_V2_UNION'
                                );

BEGIN
  FOR REC_TABLE_NAME IN CUR_TABLE_LIST
  LOOP
    BEGIN
      EXECUTE IMMEDIATE 'DROP TABLE '||REC_TABLE_NAME.TABLE_NAME;
    END;
  END LOOP;
END;
/