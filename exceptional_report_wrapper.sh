#!/usr/bin/sh
#***************************************************************************************
# Maxis Telecom
# Filename: exceptional_report_wrapper.sh
# ******************	Version : 1.0 **************************************************
# Execution Syntax: nohup exceptional_report_wrapper.sh &
# Example:exceptional_report_wrapper.sh &
#****************************************************************************************

##----- Report Main Configuration File ----------------------------
#. /rptmig/Zerolution360/LOADER/CODE/init.ini
. /apps/arbor/custom/Zerolution360_V2/EXCEPTIONAL_REPORT/CODE/init.ini

##----- Program main variables section ----------------------------
pdate=`date +"%Y%m%d%H%M%S"`
main_logfile=$report_log/exceptional_report_wrapper_${pdate}.log
#echo $pdate
##*********Checking whether the current or related process already in progress or not*************

check_runp=`ps -ef | grep exceptional_report_wrapper.sh | grep -v vi |  grep -v grep | wc -l`
sleep 2
if [ ${check_runp} -gt 1  ]; then
        echo "Process exceptional_report_wrapper.sh is already in progress" >> $main_logfile
        exit 1
fi

#****************************************************************************************
# ARCHIVE PREVIOUS LOG AND REPORT FILES
#****************************************************************************************
i=0
while [[ $i -lt  ${#scan_locations[@]} ]]
do
    for file_extension in ${file_extensions[@]}
    do
        # Moving previous log files
        if [ -f ${scan_locations[$i]}/*.${file_extension} ]
        then
            if [ ${file_zip} = "TRUE" ] ; then
                gzip ${scan_locations[$i]}/*.${file_extension}
            fi
            
            if [ ${file_move} = "TRUE"  ] ; then
                mv ${scan_locations[$i]}/*.${file_extension}* ${bkp_locations[$i]}
            fi
        fi
    done

   ((i=i+1))
done

echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | Script Started" >> $main_logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Backup Existing Log and Report Files" >> $main_logfile

#******************************************CONNECTION FOR SQL************************************#
#define SQL for connect string
SQL_CONNECT=`echo $ORACLE_HOME/bin/sqlplus -s/nolog $auth_1/$auth_2@$main_inh_db`
export SQL_CONNECT
TRIM_HEAD=`echo "head -4"`
export TRIM_HEAD
TRIM_TAIL=`echo "tail -1"`
export TRIM_TAIL


#********************************************Datelogic*********************************************************#

if [ $# -eq 0 ]; then
        RUN_DATE=`echo "SELECT TO_CHAR(SYSDATE,'DD-MON-YYYY') FROM DUAL;"`
        RUN_DATE=`((echo $RUN_DATE | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	echo " RUN_DATE : $RUN_DATE "

elif [ $# -eq 1  ] ; then	
	RUN_DATE=`echo "SELECT TO_CHAR('$1','DD-MON-YYYY') FROM DUAL;"`
	RUN_DATE=`((echo $RUN_DATE | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
        echo " RUN_DATE : $RUN_DATE "

else 
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | Invalid number of arguments passed" >> $main_logfile
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Terminated" >> $main_logfile

    	exit 1
fi

#**************************Obtaining GSMs Names******************************************
server_num=5
for db_conn in ${main_db_links[@]}
do
    db_links[$server_num]=`echo $db_conn`
    gsm_links_process[${server_num}-5]=$!
    ((server_num=server_num+1))
done

wait_process 'batch_gsm_links' $main_logfile ${gsm_links_process[@]}

#****************************************************************************************
# Program Start
#****************************************************************************************
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | Maxis Extraction Batch Execution - Started" >> $main_logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Batch Data Extraction in GSM DB" >> $main_logfile

# Batch Extraction for GSM_QERY
#****************************************************************************************
## GSM PROCESSING
#****************************************************************************************
server_num=5
for db_conn in ${main_db_names[@]}
do
    . $report_code/mxs_exceptional_report_batch_exe.sh GSM_QUERY $db_conn $main_inh_db $RUN_DATE &
    gsm_query_process[${server_num}-5]=$!
    ((server_num=server_num+1))
done

wait_process 'batch_query_gsm' $main_logfile ${gsm_query_process[@]}

#****************************************************************************************
## INHOUSE PROCESSING (RESULT CHECK)
#****************************************************************************************
echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Batch Data Extraction in MXPINH" >> $main_logfile
. $report_code/mxs_exceptional_report_batch_exe.sh RESULT_CHECK $main_inh_db $RUN_DATE ${db_links[5]} ${db_links[6]} ${db_links[7]} ${db_links[8]} ${db_links[9]} &

result_check_process=$!
wait_process 'result_check_mxpinh' $main_logfile ${result_check_process}

#****************************************************************************************
## INHOUSE PROCESSING (EXTRACTION)
#****************************************************************************************
echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Extracting Output Reports from MXPINH" >> $main_logfile
. $report_code/mxs_exceptional_report_extraction_exe.sh EXTRACTION $main_inh_db &

report_extract_process=$!
wait_process 'report_extraction' $main_logfile ${report_extract_process}

echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Maxis Extraction Batch Execution - Completed" >> $main_logfile


# Program Completed
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Completed" >> $main_logfile
#***************************************************************************
# END OF EXTRACTION PROGRAM
#***************************************************************************
